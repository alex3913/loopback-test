import {ApplicationConfig} from '@loopback/core';
import {MuukfitBackend} from './application';

export {MuukfitBackend};

export async function main(options: ApplicationConfig = {}) {
  const app = new MuukfitBackend(options);
  await app.boot();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`You can find the explorer at ${url}/explorer`);

  return app;
}
