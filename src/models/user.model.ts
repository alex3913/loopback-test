import {
  Entity,
  model,
  property,
} from '@loopback/repository';

@model({
  settings: {
    hiddenProperties: ['password'],
    lowerCaseProperties: ['email', 'firstName', 'lastName'],
    uniqueProperties: ['email', 'userName'],
  },
})
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  firstName: string;

  @property({
    type: 'string',
    required: true,
  })
  lastName: string;

  @property({
    type: 'string',
    required: true,
  })
  fullName: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      pattern: '^[a-zA-Z0-9_]+$',
    },
  })
  userName: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      format: 'email',
    },
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
