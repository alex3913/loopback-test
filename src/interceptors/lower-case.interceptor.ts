import {
  bind,
  Interceptor,
  InvocationContext,
  InvocationResult,
  Provider,
  ValueOrPromise,
} from '@loopback/context';

/**
 * This class will be bound to the application as an `Interceptor` during
 * `boot`
 */
@bind({tags: {key: LowerCaseInterceptor.BINDING_KEY}})
export class LowerCaseInterceptor implements Provider<Interceptor> {
  static readonly BINDING_KEY = `interceptors.${LowerCaseInterceptor.name}`;

  /*
  constructor() {}
  */

  /**
   * This method is used by LoopBack context to produce an interceptor function
   * for the binding.
   *
   * @returns An interceptor function
   */
  value() {
    return this.intercept.bind(this);
  }

  /**
   * The logic to intercept an invocation
   * @param invocationCtx - Invocation context
   * @param next - A function to invoke next interceptor or the target method
   */
  async intercept(
    invocationCtx: InvocationContext,
    next: () => ValueOrPromise<InvocationResult>,
  ) {
    if (
      invocationCtx.methodName === 'create' ||
      invocationCtx.methodName === 'updateById'
    ) {
      const repositoryName = Object.keys(invocationCtx.target)[0];
      const repository = (invocationCtx.target as any)[repositoryName];
      const properties: string[] = repository.modelClass.settings.lowerCaseProperties;

      if (invocationCtx.methodName === 'create')
        invocationCtx.args[0] = await this.lowerCaseArg(properties, invocationCtx.args[0]);
      else if (invocationCtx.methodName === 'updateById')
        invocationCtx.args[1] = await this.lowerCaseArg(properties, invocationCtx.args[1]);
    }

    return next();
  }

  private async lowerCaseArg(properties: string[], arg: any) {
    const argKeys = Object.keys(arg);
    properties.forEach(property => {
      if (argKeys.includes(property))
        arg[property] = arg[property].toLowerCase();
    });
    return arg;
  }
}
