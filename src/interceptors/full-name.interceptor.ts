import {
  bind,
  Interceptor,
  InvocationContext,
  InvocationResult,
  Provider,
  ValueOrPromise,
} from '@loopback/context';

/**
 * This class will be bound to the application as an `Interceptor` during
 * `boot`
 */
@bind({tags: {key: FullNameInterceptor.BINDING_KEY}})
export class FullNameInterceptor implements Provider<Interceptor> {
  static readonly BINDING_KEY = `interceptors.${FullNameInterceptor.name}`;

  /*
  constructor() {}
  */

  /**
   * This method is used by LoopBack context to produce an interceptor function
   * for the binding.
   *
   * @returns An interceptor function
   */
  value() {
    return this.intercept.bind(this);
  }

  /**
   * The logic to intercept an invocation
   * @param invocationCtx - Invocation context
   * @param next - A function to invoke next interceptor or the target method
   */
  async intercept(
    invocationCtx: InvocationContext,
    next: () => ValueOrPromise<InvocationResult>,
  ) {
    if (
      invocationCtx.methodName === 'create' ||
      invocationCtx.methodName === 'updateById'
    ) {
      if (invocationCtx.methodName === 'create') {
        const instance = invocationCtx.args[0];
        instance.fullName = `${instance.firstName} ${instance.lastName}`;
      }
      else if (invocationCtx.methodName === 'updateById') {
        const repositoryName = Object.keys(invocationCtx.target)[0];
        const repository = (invocationCtx.target as any)[repositoryName];
        const id = invocationCtx.args[0];
        const instance = invocationCtx.args[1];

        instance.fullName = await this.fullName(id, instance, repository);
      }
    }

    return next();
  }

  private async fullName(id: any, instance: any, repository: any) {
    let fullName;

    if (instance.firstName && instance.lastName)
      fullName = `${instance.firstName} ${instance.lastName}`;
    else if (instance.firstName || instance.lastName) {
      const currentInstance = await repository.findById(id, {
        fields: {
          firstName: true,
          lastName: true,
        },
      });

      if (instance.firstName)
        fullName = `${instance.firstName} ${currentInstance.lastName}`;
      else
        fullName = `${currentInstance.firstName} ${instance.lastName}`;
    }

    return fullName;
  }
}
