import {
  bind,
  Interceptor,
  InvocationContext,
  InvocationResult,
  Provider,
  ValueOrPromise,
} from '@loopback/context';

/**
 * This class will be bound to the application as an `Interceptor` during
 * `boot`
 */
@bind({tags: {key: ValidateUniqueInterceptor.BINDING_KEY}})
export class ValidateUniqueInterceptor implements Provider<Interceptor> {
  static readonly BINDING_KEY = `interceptors.${ValidateUniqueInterceptor.name}`;

  /*
  constructor() {}
  */

  /**
   * This method is used by LoopBack context to produce an interceptor function
   * for the binding.
   *
   * @returns An interceptor function
   */
  value() {
    return this.intercept.bind(this);
  }

  /**
   * The logic to intercept an invocation
   * @param invocationCtx - Invocation context
   * @param next - A function to invoke next interceptor or the target method
   */
  async intercept(
    invocationCtx: InvocationContext,
    next: () => ValueOrPromise<InvocationResult>,
  ) {
    if (
      invocationCtx.methodName === 'create' ||
      invocationCtx.methodName === 'updateById'
    ) {
      let errors: string[] = [];
      const repositoryName = Object.keys(invocationCtx.target)[0];
      const repository = (invocationCtx.target as any)[repositoryName];
      const properties: string[] = repository.modelClass.settings.uniqueProperties;

      if (invocationCtx.methodName === 'create')
        errors = await this.validateUniqueProperties(properties, invocationCtx.args[0], repository);
      else if (invocationCtx.methodName === 'updateById') {
        const id = invocationCtx.args[0];
        errors = await this.validateUniqueProperties(properties, invocationCtx.args[1], repository, id);
      }

      if (errors.length) this.validationError(errors);
    }

    return next();
  }

  private async validateUniqueProperties(properties: string[], arg: any, repository: any, id?: any) {
    const errors: string[] = [];
    const argKeys = Object.keys(arg);

    for (const property of properties) {
      if (argKeys.includes(property)) {
        const data = await repository.count({
          [property]: arg[property],
          id: {nin: [id]},
        });
        if (data.count) errors.push(property);
      }
    }

    return errors;
  }

  private validationError(errors: string[]) {
    const err: ValidationError = new ValidationError(
      `The request body is invalid. See error object \`details\` property for more info.`,
    );
    err.name = 'UnprocessableEntityError';
    err.code = 'VALIDATION_FAILED';
    err.statusCode = 422;
    err.details = [
      {
        path: '',
        code: 'uniqueness',
        message: 'some properties are NOT unique in the collection',
        info: {
          uniqueness: errors,
        },
      },
    ];
    throw err;
  }
}

class ValidationError extends Error {
  statusCode?: number;
  code?: string;
  details?: any;
}
